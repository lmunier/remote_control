.. Remote Control documentation master file, created by
   sphinx-quickstart on Wed Sep  7 09:36:39 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Remote Control's documentation!
==========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`


Table of Contents
=================

.. toctree::
   :maxdepth: 2

   api/library_root

