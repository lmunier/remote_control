/**
 * \file pins.hpp
 * \author Munier Louis
 * \brief h file to group all the used pin declarations.
 * \version 0.1
 * \date 2020-11-03
 * 
 * \copyright Copyright (c) 2020
 * 
 */

#ifndef PINS_HPP
#define PINS_HPP

/**
 * \brief esp32 devkit, joycon analog pins
 * 
 */
#define PIN_VERT 2
#define PIN_HORI 13
#define PIN_BUTT 32

#endif //PINS_HPP
