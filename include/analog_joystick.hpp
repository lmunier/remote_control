/**
 * \file analog_joystick.hpp
 * \author Munier Louis
 * \brief Class to have all the needed tools to use analog joysticks in an arduino
 * project.
 * \version 0.1
 * \date 2020-11-06
 *
 * \copyright Copyright (c) 2020
 *
 */

#ifndef ANALOG_JOYSTICK_HPP
#define ANALOG_JOYSTICK_HPP

#include "FS.h"
#include "SPIFFS.h"
#include "Arduino.h"
#include <string>
#include "pins.hpp"

#define TIMER_0 0
#define PRESCALER 80
#define PERIOD_UPDATE 10000 // Equivalent to have 100 Hz refresh rate

#define PERC_MIN_THRESHOLD 1.2
#define PERC_MAX_THRESHOLD 0.8

#define EPS_VALUE 2
#define DEFAULT_THRESH_MIN_RIGHT 1500
#define DEFAULT_THRESH_MAX_RIGHT 850
#define DEFAULT_THRESH_MIN_UP 950
#define DEFAULT_THRESH_MAX_UP 1350
#define DEFAULT_THRESH_MIN_LEFT 2050
#define DEFAULT_THRESH_MAX_LEFT 2800
#define DEFAULT_THRESH_MIN_DOWN 600
#define DEFAULT_THRESH_MAX_DOWN 450

#define NB_CALIB_VALUES 8
#define MEAN_SIZE 10
#define FORMAT_SPIFFS_IF_FAILED true

/**
 * Class of nintendo switch joystick tools.
 *
 * \class SwitchJoy
 */
class AnalogJoystick
{
public:
    /**
     * \brief Construct a new Switch Joy object.
     *
     * \param timer
     * \param path
     * \param load
     *
     */
    AnalogJoystick(hw_timer_t *timer, String path = "", bool load = false);

    /**
     * \brief Check if the calibration is already done.
     *
     * \return true if calibration done
     * \return false if calibration not done
     */
    bool is_calibrate() { return _calibration_state; }

    /**
     * \brief Calibrate joycon to well define different thresholds.
     *
     * \return true if the calibration successed.
     * \return false if the calibration failed.
     */
    bool calibrate();

    /**
     * \brief Store a calibration file to retrieve it later.
     *
     * \param fs
     * \param path
     * \param timer
     *
     * \return true if it is saved successfully.
     * \return false if it is not able to save.
     */
    bool store_calibration(fs::FS &fs, String path, hw_timer_t *timer);

    /**
     * \brief Load a calibration file to retrieve last configuration. If argument
     * is NULL, try to find the last modified file.
     *
     * \param fs
     * \param path
     * \param timer
     *
     * \return true if it is loaded successfully.
     * \return false if it is not able to find a calibration file.
     */
    bool load_calibration(fs::FS &fs, String path, hw_timer_t *timer);

    /**
     * \brief Update values retrieve from horizontal and vertical joycon axis.
     *
     */
    void update_values();

    /**
     * \brief Read values returned by the horizontal and vertical joycon axis.
     *
     * \param val_horiz average horizontal value of the joycon position.
     * \param val_vert average vertical value of the joycon position.
     */
    void read_axis(int *val_horiz, int *val_vert);

    /**
     * \brief Read values returned by the chosen joycon axis.
     *
     * \param axis_name name of the axis (horizontal / vertical)
     *
     */
    int read_axis(String axis_name);

    /**
     * \brief Read value returned by the joycono button
     *
     * \return int
     */
    int read_button() { return analogRead(PIN_BUTT); }

    /**
     * \brief Reset default threshold values.
     *
     */
    void _reset_thresholds();

    bool _calibration_state = false;
    int _tab_calibration[NB_CALIB_VALUES] = {0};

private:
    /**
     * \brief Indexes and arrays to well compute mobile average.
     *
     */
    int _idx_horiz = 0;
    int _idx_vert = 0;

    int _tab_horiz[MEAN_SIZE] = {0};
    int _tab_vert[MEAN_SIZE] = {0};

    /**
     * \brief Return an average on last MEAN_SIZE values.
     *
     * \return int
     */
    int _mobile_average(int[]);
};

#endif // ANALOG_JOYSTICK_HPP
