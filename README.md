# Some notes about switch joycon

This file contains some notes about using a joycon switch.

- *Written by Munier Louis, last update 2020.11.05*

## Table of Contents

1. [Documentation](#documentation)  
1. [Bill of Material](#bill-of-material)  
1. [Code](#code)  
1. [Tests](#tests)

## Documentation

The main documentation for this part comes from the following tutorial :

- Timer alarm and interrupts : <https://diyprojects.io/esp32-timers-alarms-interrupts-arduino-code/>

## Bill of Material

The material used to quickly test the joycon switch of this project is the following one :

- 1x ESP32 devkit v1  
- 1x analog Joycon switch

## Code

The code is located inside ...

## Tests

Tests were convincing but some bugs needed to be documentate. They are in this section.

### BUG - SPIFFS + Timers

Uisng SPIFFS access when a timer is running provokes crashes of the ESP32. It is because they interfer between each other :
<https://github.com/espressif/arduino-esp32/issues/2942>

The solution used here is to stop the timer when accessing SPIFFS part since it is done only to read/write a calibration file inside memory.
