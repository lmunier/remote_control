/**
 * \file analog_joystick.cpp
 * \author Munier Louis
 * \brief File to have all tools needed to well manage nintendo switch joycon
 * and retrieve acceptable values for the project.
 * \version 0.1
 * \date 2020-11-06
 * 
 * \copyright Copyright (c) 2020
 * 
 */

#include "analog_joystick.hpp"

AnalogJoystick::AnalogJoystick(hw_timer_t* timer, String path, bool load) {
    bool loading = false;

    if (load) {
        loading = this->load_calibration(SPIFFS, path, timer);
        this->_calibration_state = true;
    } else {
        this->_reset_thresholds();
        this->_calibration_state = false;
    }
}

bool AnalogJoystick::calibrate() {
    int new_value = 0, first_value = 0, second_value = 0;
    String state_name[] = {"max", "zero"};
    String axis_name[] = {"horizontal", "vertical"};
    String axis[] = {"right", "up", "left", "down"};

    for (int i=0; i<NB_CALIB_VALUES; i++) {
        first_value = 0;
        second_value = 0;

        printf("Move %s to %s\n", axis[(int)(i / 2)].c_str(), state_name[i % 2].c_str());
        
        if (state_name[i % 2] == "zero") {
            if (this->_tab_calibration[i - 1] > this->_tab_calibration[i]) {
                do {
                    delay(500);
                    new_value = this->read_axis(axis_name[(int)((i / 2) % 2)]);
                } while (new_value < this->_tab_calibration[i - 1]);
            } else {
                do {
                    delay(500);
                    new_value = this->read_axis(axis_name[(int)((i / 2) % 2)]);
                } while (new_value > this->_tab_calibration[i - 1]);
            }
        } else {
            if (this->_tab_calibration[i] > this->_tab_calibration[i + 1]) {
                do {
                    delay(500);
                    new_value = this->read_axis(axis_name[(int)((i / 2) % 2)]);
                } while (new_value > this->_tab_calibration[i + 1]);
            } else {
                do {
                    delay(500);
                    new_value = this->read_axis(axis_name[(int)((i / 2) % 2)]);
                } while (new_value < this->_tab_calibration[i + 1]);
            }
        }

        do {
            first_value = this->read_axis(axis_name[(int)((i / 2) % 2)]);
            delay(50);
            second_value = this->read_axis(axis_name[(int)((i / 2) % 2)]);
        } while (abs(first_value - second_value) > EPS_VALUE);

        if (state_name[i % 2] == "max") {
            this->_tab_calibration[i + 1] = second_value;
        } else {
            this->_tab_calibration[i - 1] = second_value;
        }

        printf("Done\n");
    }

    this->_calibration_state = true;
    return true;
}

bool AnalogJoystick::store_calibration(fs::FS &fs, String path, hw_timer_t* timer) {
    timerStop(timer);
    String to_store = "";
    File file = fs.open(path, FILE_WRITE);

    if (!file) {
        printf("There was an error opening the file for writing.\n");
        return false;
    }

    for (int i=0; i<NB_CALIB_VALUES; i++) {
        if (!file.print(this->_tab_calibration[i])) {
            return false;
        }

        if (i == NB_CALIB_VALUES - 1) {
            if (!file.print("\n")) {
                return false;
            }
        } else {
            if (!file.print(" ")) {
                return false;
            }
        }
    }

    file.close();
    timerStart(timer);
    return true;
}

bool AnalogJoystick::load_calibration(fs::FS &fs, String path, hw_timer_t* timer) {
    timerStop(timer);

    int idx_number = 0;
    String c = "", number = "";
    File file = fs.open(path);

    if (!file) {
        printf("There was an error opening the file for reading.\n");
        return false;
    }

    while(file.available()) {
        c = file.read();

        if (c != " " && c != "\n") {
            number += c;
        }
        
        if (number == "") {
            this->_tab_calibration[idx_number] = number.toInt();

            number = "";
            idx_number++;
        }
    }

    file.close();
    timerStart(timer);
    return true;
}

void AnalogJoystick::update_values() {
    this->_tab_horiz[this->_idx_horiz] = analogRead(PIN_HORI);
    this->_idx_horiz = (this->_idx_horiz + 1) % MEAN_SIZE;

    this->_tab_vert[this->_idx_vert] = analogRead(PIN_VERT);
    this->_idx_vert = (this->_idx_vert + 1) % MEAN_SIZE;
}

void AnalogJoystick::read_axis(int* val_horiz, int* val_vert) {
    *val_horiz = this->_mobile_average(this->_tab_horiz);
    *val_vert = this->_mobile_average(this->_tab_vert);
}

int AnalogJoystick::read_axis(String axis_name) {
    if (axis_name == "horizontal") {
        return this->_mobile_average(this->_tab_horiz);
    } else if (axis_name == "vertical") {
        return this->_mobile_average(this->_tab_vert);
    }

    return -1; // TODO check if it is an error
}

int AnalogJoystick::_mobile_average(int values[]) {
    int sum = 0;

    for (int i=0; i<MEAN_SIZE; i++)
        sum += values[i];

    return int(sum / MEAN_SIZE);
}

void AnalogJoystick::_reset_thresholds() {
    this->_tab_calibration[0] = DEFAULT_THRESH_MIN_RIGHT;
    this->_tab_calibration[1] = DEFAULT_THRESH_MAX_RIGHT;
    this->_tab_calibration[2] = DEFAULT_THRESH_MIN_UP;
    this->_tab_calibration[3] = DEFAULT_THRESH_MAX_UP;
    this->_tab_calibration[4] = DEFAULT_THRESH_MIN_LEFT;
    this->_tab_calibration[5] = DEFAULT_THRESH_MAX_LEFT;
    this->_tab_calibration[6] = DEFAULT_THRESH_MIN_DOWN;
    this->_tab_calibration[7] = DEFAULT_THRESH_MAX_DOWN;
}
