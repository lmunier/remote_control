/**
 * \file first_switch_joy.ino
 * \author Munier Louis
 * \brief Small arduino project to test switch_joy implementation and be sure
 * to be able to use it in other projects.
 * \version 0.1
 * \date 2020-11-06
 * 
 * \copyright Copyright (c) 2020
 * 
 */

#include "SPIFFS.h"
#include <stdio.h>
#include "Arduino.h"
#include "pins.hpp"
#include "analog_joystick.hpp"

hw_timer_t* timer = NULL;
portMUX_TYPE timerMux = portMUX_INITIALIZER_UNLOCKED;

AnalogJoystick joy = AnalogJoystick(timer);
int val_hori = 0, val_vert = 0;

void update_joy() {
    joy.update_values();
}

void setup() {
    Serial.begin(115200);

    if (!SPIFFS.begin(FORMAT_SPIFFS_IF_FAILED)) {
        Serial.write("An error ocurred while mounting SPIFFS.");
        return;
    }

    // Configure the Prescaler at 80 the quarter of the ESP32 is cadence at 80Mhz
    // 80000000 / 80 = 1000000 tics / seconde
    timer = timerBegin(TIMER_0, PRESCALER, true);
    timerAttachInterrupt(timer, &update_joy, true);

    // Sets an alarm to a certain PERIOD_UPDATE
    timerAlarmWrite(timer, PERIOD_UPDATE, true);
    timerAlarmEnable(timer);

    delay(5000);
}

void loop() {
    /*if (joy.read_button() <= EPS_VALUE)
        joy.calibrate();

    if (joy.is_calibrate()) {
        for (int i=0; i<NB_CALIB_VALUES; i++)
            printf("%d ", joy._tab_calibration[i]);
        
        printf("Done !");
        delay(5000);
    }*/

    if (!joy.is_calibrate()) {
        joy.calibrate();
        joy.store_calibration(SPIFFS, "/calib_joycon.txt", timer);
    }

    if (joy.read_button() <= EPS_VALUE) {
        joy.load_calibration(SPIFFS, "/calib_joycon.txt", timer);

        for (int i=0; i<NB_CALIB_VALUES; i++) {
            printf("%d ", joy._tab_calibration[i]);
        }
    }

    delay(500);
}
